#pragma once

#include "assignment_filter_helper.h"
#include "device_matrix.h"

#include <memory>

//! A class that encapsulates the functionality of the assignment filter.
//! The computations are done with single precision.
class assignment_filter final
{
	//! Enables using std::make_unique with the named constructor idiom.
	struct ctor_cookie
	{
	};

  public:
	//! Default destructor.
	~assignment_filter() = default;

	//! A static member function that creates the assignment class.
	/*!
	 * \param i_param The assignment parameters.
	 * \param i_term The termination criteria.
	 * \return A smart pointer to the assignment filter class.
	 */
	static std::unique_ptr<assignment_filter> create(assignment_param i_param = assignment_param{},
													 term_criteria i_term = term_criteria{});

	//! A member function that applies the assignment filter to an image.
	/*!
	 * \param i_image The input image.
	 * \param i_labels The labels.
	 * \return The input image with the filter applied.
	 */
	device_matrix<vec3<float>> compute(const device_matrix<vec3<float>>& i_image,
									   const thrust::device_vector<vec3<float>>& i_labels);

	//! A member function that computes the assignment probabilites for a given input image.
	/*!
	 * \param i_D The distance matrix. A row contains the distances of a pixel to each label. The
	 * matrix \param i_size_image The image dimension corresponding to the distance matrix. \return
	 * The assignment probabilities. A row contains the probabilities of assigning each label to a
	 * pixel.
	 */
	device_matrix<float> compute_assignment_prob(const device_matrix<float>& i_D,
												 const Size& i_size_image);

	//! A member function that computes the distance matrix for a given input image.
	/*!
	 * \param i_image Input image.
	 * \param i_labels The labels.
	 * \return The distance matrix. A row contains the distance of a pixel to each label.
	 */
	device_matrix<float>
		compute_distance_matrix(const device_matrix<vec3<float>>& i_image,
								const thrust::device_vector<vec3<float>>& i_labels) const;

	//! A member function that computes the adjacency matrix for the given image size.
	/*!
	 * The neighborhood size can be set via the member function set_assignment_parameter().
	 * \param i_size_image The image size.
	 * \return The adjacency matrix. Each row contains the weights for one pixel to each other
	 * pixel.
	 */
	device_csr_matrix<float> compute_adj_matrix(const Size& i_size_image) const;

	//! A member function that sets the assignment parameters.
	/*!
	 * \param i_param The assignment parameters.
	 */
	void set_assignment_parameter(assignment_param i_param);

	//! A member function that sets the termination criteria.
	/*!
	 * \param i_term The termination criteria.
	 */
	void set_termination_criteria(term_criteria i_term);

	//! A static member function that returns the assignment parameters.
	/*!
	 * \return i_term The termination criteria.
	 */
	assignment_param get_assignment_param() const noexcept;

	//! A static member function that returns the termination criteria.
	/*!
	 * \return i_term The termination criteria.
	 */
	term_criteria get_termination_criteria() const noexcept;

	//! The constructor can not to be used publicly.
	/*!
	 * \param assignment_param The assignment parameters.
	 * \param i_term The termination criteria.
	 * \param ctor_cookie Prevents calling the constructor publicly.
	 */
	explicit assignment_filter(assignment_param i_param, term_criteria i_term, ctor_cookie);

  private:
	device_matrix<float> initialize(const device_matrix<float>& i_D, const Size& i_size_image);

	unique_cusparse_handle m_sparse_handle{};
	unique_cusparse_descriptor m_sparse_descr{};

	assignment_param m_param{};
	term_criteria m_term{};
	bool m_refresh_avr_matrix{false};

	device_matrix<float> m_U{};
	device_matrix<float> m_L{};
	device_matrix<float> m_S{};

	device_csr_matrix<float> m_avr_matrix{};
	thrust::device_vector<float> m_avr_row{};
};

//! A class that encapsulates the functionality of the assignment filter.
//! The computations are done with double precision.
class assignment_filter_dbl final
{
	//! Enables using std::make_unique with the named constructor idiom.
	struct ctor_cookie
	{
	};

  public:
	//! Default destructor.
	~assignment_filter_dbl() = default;

	//! A static member function that creates the assignment class.
	/*!
	 * \param i_param The assignment parameters.
	 * \param i_term The termination criteria.
	 * \return A smart pointer to the assignment filter class.
	 */
	static std::unique_ptr<assignment_filter_dbl>
		create(assignment_param i_param = assignment_param{},
			   term_criteria i_term = term_criteria{});

	//! A member function that applies the assignment filter to an image.
	/*!
	 * \param i_image The input image.
	 * \param i_labels The labels.
	 * \return The input image with the filter applied.
	 */
	device_matrix<vec3<float>> compute(const device_matrix<vec3<float>>& i_image,
									   const thrust::device_vector<vec3<float>>& i_labels);

	//! A member function that computes the assignment probabilites for a given input image.
	/*!
	 * \param i_D The distance matrix. A row contains the distances of a pixel to each label. The
	 * matrix \param i_size_image The image dimension corresponding to the distance matrix. \return
	 * The assignment probabilities. A row contains the probabilities of assigning each label to a
	 * pixel.
	 */
	device_matrix<double> compute_assignment_prob(const device_matrix<double>& i_D,
												  const Size& i_size_image);

	//! A member function that computes the distance matrix for a given input image.
	/*!
	 * \param i_image Input image.
	 * \param i_labels The labels.
	 * \return The distance matrix. A row contains the distance of a pixel to each label.
	 */
	device_matrix<double>
		compute_distance_matrix(const device_matrix<vec3<float>>& i_image,
								const thrust::device_vector<vec3<float>>& i_labels) const;

	//! A member function that computes the adjacency matrix for the given image size.
	/*!
	 * The neighborhood size can be set via the member function set_assignment_parameter().
	 * \param i_size_image The image size.
	 * \return The adjacency matrix. Each row contains the weights for one pixel to each other
	 * pixel.
	 */
	device_csr_matrix<double> compute_adj_matrix(const Size& i_size_image) const;

	//! A member function that sets the assignment parameters.
	/*!
	 * \param i_param The assignment parameters.
	 */
	void set_assignment_parameter(assignment_param i_param);

	//! A member function that sets the termination criteria.
	/*!
	 * \param i_term The termination criteria.
	 */
	void set_termination_criteria(term_criteria i_term);

	//! A static member function that returns the assignment parameters.
	/*!
	 * \return i_term The termination criteria.
	 */
	assignment_param get_assignment_param() const noexcept;

	//! A static member function that returns the termination criteria.
	/*!
	 * \return i_term The termination criteria.
	 */
	term_criteria get_termination_criteria() const noexcept;

	//! The constructor can not to be used publicly.
	/*!
	 * \param assignment_param The assignment parameters.
	 * \param i_term The termination criteria.
	 * \param ctor_cookie Prevents calling the constructor publicly.
	 */
	explicit assignment_filter_dbl(assignment_param i_param, term_criteria i_term, ctor_cookie);

  private:
	device_matrix<double> initialize(const device_matrix<double>& i_D, const Size& i_size_image);

	unique_cusparse_handle m_sparse_handle{};
	unique_cusparse_descriptor m_sparse_descr{};

	assignment_param m_param{};
	term_criteria m_term{};
	bool m_refresh_avr_matrix{false};

	device_matrix<double> m_U{};
	device_matrix<double> m_L{};
	device_matrix<double> m_S{};

	device_csr_matrix<double> m_avr_matrix{};
	thrust::device_vector<double> m_avr_row{};
};