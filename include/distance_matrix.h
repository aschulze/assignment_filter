#pragma once

#include "device_matrix.h"
#include "metric.h"

#include "../extern/gsl/gsl_assert"
#include "../extern/gsl/gsl_util"

#include <thrust/device_vector.h>

#include <cuda_runtime.h> // CUDA C++ API
#include <device_launch_parameters.h>

#include <exception>

//! An overloaded function that computes the distance between each element of two vectors.
/*
 * \param i_A First input vector.
 * \param i_B Second input vector.
 * \return A matrix which i-th row contains the distances of the i-th element in i_A to every
 * element in i_B.
 */
template<typename Metric = euclidean_metric, typename T>
device_matrix<float> distance_matrix(const thrust::device_vector<T>& i_A,
									 const thrust::device_vector<T>& i_B)
{
	Expects(i_A.size() >= i_B.size());

	// Get raw pointers and variables needed in the device function because lambda device functions
	// have to catch by value.
	const auto size_a = i_A.size();
	const auto size_b = i_B.size();
	const auto A_ptr = i_A.data().get();
	const auto B_ptr = i_B.data().get();

	// Output distances between patches.
	auto o_distances = device_matrix<float>{Rows{size_a}, Cols{size_b}};
	auto distances_ptr = o_distances.data().get();

	const auto first_pixel = thrust::counting_iterator<std::size_t>{0};
	const auto last_pixel = thrust::counting_iterator<std::size_t>{size_a};

	thrust::for_each(first_pixel, last_pixel, [=] __device__(std::size_t row) {
		for(std::size_t i = 0; i < size_b; i++)
		{
			distances_ptr[i * size_a + row] =
				static_cast<float>(Metric::compute(A_ptr[row], B_ptr[i]));
		}
	});

	return o_distances;
}

//! An overloaded function that computes the distance between each element of both inputs.
/*
 * \param i_A A matrix.
 * \param i_B A vector.
 * \return A matrix which i-th row contains the distances of the i-th element in i_A to every
 * element in i_B. The i-th element in i_A is defined to be i-th element in the underlying vector.
 */
template<typename Metric = euclidean_metric, typename T>
device_matrix<float> distance_matrix(const device_matrix<T>& i_A,
									 const thrust::device_vector<T>& i_B)
{
	Expects(i_A.rows() * i_A.cols() >= i_B.size());

	// Get raw pointers and variables needed in the device function because lambda device functions
	// have to catch by value.
	const auto size_a = i_A.total();
	const auto size_b = i_B.size();
	const auto A_ptr = i_A.data().get();
	const auto B_ptr = i_B.data().get();

	// Output distances between patches.
	auto o_distances = device_matrix<float>{Rows{size_a}, Cols{size_b}};
	auto distances_ptr = o_distances.data().get();

	const auto first_pixel = thrust::counting_iterator<std::size_t>{0};
	const auto last_pixel = thrust::counting_iterator<std::size_t>{size_a};

	thrust::for_each(first_pixel, last_pixel, [=] __device__(std::size_t row) {
		for(std::size_t i = 0; i < size_b; i++)
		{
			distances_ptr[i * size_a + row] =
				static_cast<float>(Metric::compute(A_ptr[row], B_ptr[i]));
		}
	});

	return o_distances;
}

//! An overloaded function that computes the distance between each element of two vectors.
/*
 * \param i_A First input vector.
 * \param i_B Second input vector.
 * \return A matrix which i-th row contains the distances of the i-th element in i_A to every
 * element in i_B.
 */
template<typename Metric = euclidean_metric, typename T>
device_matrix<double> distance_matrix_dbl(const thrust::device_vector<T>& i_A,
										  const thrust::device_vector<T>& i_B)
{
	Expects(i_A.size() >= i_B.size());

	// Get raw pointers and variables needed in the device function because lambda device functions
	// have to catch by value.
	const auto size_a = i_A.size();
	const auto size_b = i_B.size();
	const auto A_ptr = i_A.data().get();
	const auto B_ptr = i_B.data().get();

	// Output distances between patches.
	auto o_distances = device_matrix<double>{Rows{size_a}, Cols{size_b}};
	auto distances_ptr = o_distances.data().get();

	const auto first_pixel = thrust::counting_iterator<std::size_t>{0};
	const auto last_pixel = thrust::counting_iterator<std::size_t>{size_a};

	thrust::for_each(first_pixel, last_pixel, [=] __device__(std::size_t row) {
		for(std::size_t i = 0; i < size_b; i++)
		{
			distances_ptr[i * size_a + row] =
				static_cast<double>(Metric::compute(A_ptr[row], B_ptr[i]));
		}
	});

	return o_distances;
}

//! An overloaded function that computes the distance between each element of both inputs.
/*
 * \param i_A A matrix.
 * \param i_B A vector.
 * \return A matrix which i-th row contains the distances of the i-th element in i_A to every
 * element in i_B. The i-th element in i_A is defined to be i-th element in the underlying vector.
 */
template<typename Metric = euclidean_metric, typename T>
device_matrix<double> distance_matrix_dbl(const device_matrix<T>& i_A,
										  const thrust::device_vector<T>& i_B)
{
	Expects(i_A.rows() * i_A.cols() >= i_B.size());

	// Get raw pointers and variables needed in the device function because lambda device functions
	// have to catch by value.
	const auto size_a = i_A.total();
	const auto size_b = i_B.size();
	const auto A_ptr = i_A.data().get();
	const auto B_ptr = i_B.data().get();

	// Output distances between patches.
	auto o_distances = device_matrix<double>{Rows{size_a}, Cols{size_b}};
	auto distances_ptr = o_distances.data().get();

	const auto first_pixel = thrust::counting_iterator<std::size_t>{0};
	const auto last_pixel = thrust::counting_iterator<std::size_t>{size_a};

	thrust::for_each(first_pixel, last_pixel, [=] __device__(std::size_t row) {
		for(std::size_t i = 0; i < size_b; i++)
		{
			distances_ptr[i * size_a + row] =
				static_cast<double>(Metric::compute(A_ptr[row], B_ptr[i]));
		}
	});

	return o_distances;
}