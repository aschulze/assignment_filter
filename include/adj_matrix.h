#pragma once

#include "device_matrix.h"

#include "../extern/gsl/gsl_assert"
#include "../extern/gsl/gsl_util"

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>

using namespace thrust::placeholders;

// The partition count is a critical number for kernel execution time.
// It should be higher than the number of cuda cores.
static constexpr auto PARTITIONCOUNT = 5000.0f;

//! A function returning the sum of the integer from one to n. Only positive integers are allowed.
/*!
 *	\param i_n The maximal number n to sum up to.
 */
inline __host__ __device__ constexpr int sum_n_(int i_n) noexcept
{
	return i_n * (i_n + 1) / 2;
}

//! A function returning the sum of the integer from one to n. Only positive integers are allowed.
/*!
 *	\param i_m The maximal number n to sum up to.
 *	\param i_n The maximal number n to sum up to.
 */
inline __host__ __device__ constexpr int sum_m_n(int i_m, int i_n) noexcept
{
	return sum_n_(i_n) - sum_n_(i_m - 1);
}

//! A device function that returns the starting index in the csr-matrix for the entries of a
//! neighbourhood.
/*!
 * \param i_x Column index.
 * \param i_y Row index.
 * \param i_img_rows Rows of the matrix.
 * \param i_img_cols Columns of the matrix.
 * \param i_neigh_rows Rows of the neighborhood.
 * \param i_neigh_cols Columns of the neighborhood.
 */
inline __host__ __device__ int getIndex(int i_x, int i_y, int i_img_rows, int i_img_cols,
										int i_neigh_rows, int i_neigh_cols)
{
	const auto neigh_rows_2 = (i_neigh_rows - 1) / 2;
	const auto neigh_cols_2 = (i_neigh_cols - 1) / 2;

	// We start by computing the number of entries until (and including) the complete column ix.

	// Count pixels which have the full neighborhood size.
	const auto neigh_size = i_neigh_rows * i_neigh_cols;
	const auto full_col_count = min(max(0, i_x + 1 - neigh_cols_2), i_img_cols - 2 * neigh_cols_2);
	const auto full_row_count = max(0, i_img_rows - 2 * neigh_rows_2);
	const auto entries_full_neigh = full_col_count * full_row_count * neigh_size;

	// Count pixel entries with a clamped neighborhood.
	// Edges
	// Remark: Expects image rows >= neighborhood rows - 1 and image columns >= neighborhood
	// columns
	// - 1
	const auto row_accum = sum_m_n(neigh_rows_2 + 1, i_neigh_rows - 1);
	const auto l_cols = min(neigh_cols_2, i_x + 1);
	const auto l_cols_acumm = sum_m_n(neigh_cols_2 + 1, neigh_cols_2 + l_cols);
	const auto l_edges = 2 * row_accum * l_cols_acumm;
	const auto r_cols = max(i_x + 1 - i_img_cols + neigh_cols_2, 0);
	const auto r_cols_acumm = sum_m_n(i_neigh_cols - r_cols, i_neigh_cols - 1);
	const auto r_edges = 2 * row_accum * r_cols_acumm;

	// Horizontal bounds.
	const auto l_h_bound = full_row_count * i_neigh_rows * l_cols_acumm;
	const auto r_h_bound = full_row_count * i_neigh_rows * r_cols_acumm;

	// Upper and lower bounds.
	const auto v_bounds = 2 * full_col_count * i_neigh_cols * row_accum;

	const auto total_full_cols =
		entries_full_neigh + l_edges + r_edges + l_h_bound + r_h_bound + v_bounds;

	// Next we compute the number of entries in the column ix following row iy. So we can subtract
	// it from the value computed in the previous step.
	const auto u_rows = max(0, neigh_rows_2 - i_y);
	const auto m_rows = min(i_img_rows - 2 * neigh_rows_2, max(0, i_img_rows - i_y - neigh_rows_2));
	const auto l_rows = min(neigh_rows_2, i_img_rows - i_y);

	const auto u_row_accum = sum_m_n(i_neigh_rows - u_rows, i_neigh_rows - 1);
	const auto l_row_accum = sum_m_n(neigh_rows_2 + 1, neigh_rows_2 + l_rows);

	const auto neigh_cols = neigh_cols_2 + 1 + min(min(neigh_cols_2, i_x), neigh_cols_2 - r_cols);

	const auto total_subtract =
		neigh_cols * u_row_accum + neigh_cols * l_row_accum + neigh_cols * i_neigh_rows * m_rows;

	return total_full_cols - total_subtract;
}

//! A function that returns the number of non zero elements in a for an adjacency matrix.
/*!
 * \param i_img_rows Rows of the matrix.
 * \param i_img_cols Columns of the matrix.
 * \param i_neigh_rows Rows of the neighborhood.
 * \param i_neigh_cols Columns of the neighborhood.
 */
inline __host__ std::size_t getNNZ(int i_img_rows, int i_img_cols, int i_neigh_rows,
								   int i_neigh_cols)
{
	return gsl::narrow<std::size_t>(
		getIndex(i_img_cols - 1, i_img_rows, i_img_rows, i_img_cols, i_neigh_rows, i_neigh_cols));
}

//! A CUDA kernel that returns the adjacency matrix for a given image size and a weighted
//! neighborhood.
/*!
 * \param i_img_rows Rows of the matrix.
 * \param i_img_cols Columns of the matrix.
 * \param i_neigh_ptr The data pointer to the neighborhood matrix.
 * \param i_neigh_rows Rows of the neighborhood.
 * \param i_neigh_cols Columns of the neighborhood.
 * \param i_nnz Number of non zero elements in the adjacency matrix.
 * \param i_csr_val_a The vector containing the non zero elements of the csr matrix.
 * \param i_row_ptr_a The vector containing the index to the first non zero element in
 * each row.
 * \param i_col_ind_a The vector containing the column index of each element.
 * \param i_normalize If true the rows will be normalized to one.
 */
template<typename T>
__global__ void adj_matrix_kernel(int i_img_rows, int i_img_cols, const T* i_neigh_ptr,
								  int i_neigh_rows, int i_neigh_cols, int i_nnz, T* i_csr_val_a,
								  int* i_csr_row_ptr_a, int* i_csr_col_ind_a, bool i_normalize)
{
	const auto idx = threadIdx.x;
	const auto idy = threadIdx.y;

	// Use 2-dimensional grid for computation.
	// The columns get split according to the global partition count.
	const auto ix = static_cast<int>(idx + blockIdx.x * blockDim.x); // column-index
	const auto iy_raw = static_cast<int>(idy + blockIdx.y * blockDim.y); // not row-index

	// Split the current column in equally sized partitions with exception of the last partition
	// which might be smaller.
	const auto partitions_per_col = ceil(PARTITIONCOUNT / i_img_cols);
	const auto partition_size_max = static_cast<int>(ceil(i_img_rows / partitions_per_col));
	const auto iy_begin = iy_raw * static_cast<int>(partition_size_max); // row-index
	const auto partition_size = max(0, min(partition_size_max, i_img_rows - iy_begin));

	if(iy_begin + partition_size - 1 < i_img_rows && ix < i_img_cols)
	{
		const auto neigh_rows_2 = (i_neigh_rows - 1) / 2;
		const auto neigh_cols_2 = (i_neigh_cols - 1) / 2;

		// Starting index in the csr value vector.
		auto csr_index = getIndex(ix, iy_begin, i_img_rows, i_img_cols, i_neigh_rows, i_neigh_cols);

		// The width of the neighborhood of each pixel in a partition is the same, because we stay
		// in the same column.
		const auto col_begin = max(0, ix - neigh_cols_2);
		const auto col_end = min(i_img_cols, ix + neigh_cols_2 + 1);

		// Iterate over partition size = pixel count = number of neighborhoods.
		for(int i_part = 0; i_part < partition_size; i_part++)
		{
			// Holds the index in the csr value vector, where the current row = neighborhood
			// begins.
			const auto iy = iy_begin + i_part;
			i_csr_row_ptr_a[ix * i_img_rows + iy] = csr_index;

			// The number of rows of the neighborhood might change if we go to the next pixel in a
			// column.
			const auto row_begin = max(0, iy - neigh_rows_2);
			const auto row_end = min(i_img_rows, iy + neigh_rows_2 + 1);

			// Sum of all values in the neighborhood. Used to normalize the neighborhood.
			// Arithmetic peak performance is much higher than peak memory bandwidth. Therefore
			// fusing multiple kernels instead of separate launches is often faster where
			// applicable. So the sum will be computed regardless of whether to normalize or not.
			auto sum = T{};
			const auto csr_index_begin = csr_index;

			// Iterate over the non zero entries in the neighborhood matrix.
			for(int j = col_begin; j < col_end; j++)
			{
				for(int i = row_begin; i < row_end; i++)
				{
					const auto w_row = i - iy + neigh_rows_2;
					const auto w_col = j - ix + neigh_cols_2;
					const auto neigh_val = i_neigh_ptr[i_neigh_rows * w_col + w_row];

					i_csr_val_a[csr_index] = neigh_val;
					i_csr_col_ind_a[csr_index] = i_img_rows * j + i;

					sum += neigh_val;

					csr_index++;
				}
			}

			if(i_normalize)
			{
				for(int i = csr_index_begin; i < csr_index; i++)
				{
					i_csr_val_a[i] = i_csr_val_a[i] / sum;
				}
			}
		}
	}

	// The last entry in the csr row vector is the number of non zero elements (plus zero for
	// zero-based indexing).
	if(ix == 0 && iy_begin == 0)
		i_csr_row_ptr_a[i_img_cols * i_img_rows] = i_nnz;
}

//! A function that returns the adjacency matrix for a given image size and a weighted
//! neighborhood.
/*!
 * The computation is done via CUDA.
 * \param i_img The image size.
 * \param i_neigh The neighborhood matrix containing the weights for each element.
 * \param i_normalize If true the rows will be normalized to one.
 */
template<typename T>
device_csr_matrix<T> adj_matrix(const Size& i_img, const device_matrix<T>& i_neigh,
								bool i_normalize)
{
	Expects(i_neigh.rows() < i_img.rows());
	Expects(i_neigh.cols() < i_img.cols());
	Expects(i_neigh.rows() % 2 == 1);
	Expects(i_neigh.cols() % 2 == 1);
	Expects(i_neigh.ld() == i_neigh.rows());

	const auto img_rows = gsl::narrow<int>(i_img.rows());
	const auto img_cols = gsl::narrow<int>(i_img.cols());
	const auto img_cols_f = gsl::narrow_cast<float>(i_img.cols());
	const auto neigh_rows = gsl::narrow<int>(i_neigh.rows());
	const auto neigh_cols = gsl::narrow<int>(i_neigh.cols());

	// The number of non-zero elements in the csr matrix.
	const auto nnz = getNNZ(img_rows, img_cols, neigh_rows, neigh_cols);

	auto o_adj = device_csr_matrix<T>{Rows{i_img.total()}, Cols{i_img.total()}, nnz};

	const auto partitions_per_col = gsl::narrow<int>(std::ceil(PARTITIONCOUNT / img_cols_f));
	const auto block = dim3{32, 16};
	const auto grid =
		dim3{(img_cols + block.x - 1) / block.x, (partitions_per_col + block.y - 1) / block.y};

	adj_matrix_kernel<<<block, grid>>>(img_rows, img_cols, i_neigh.data().get(), neigh_rows,
									   neigh_cols, gsl::narrow<int>(nnz), o_adj.value_data().get(),
									   o_adj.row_ptr_data().get(), o_adj.col_ind_data().get(),
									   i_normalize);

	// Check if the kernel execution generated an error.
	const auto status = cudaDeviceSynchronize();
	if(status != cudaSuccess)
		throw thrust::system_error{status, thrust::cuda_category()};

	return o_adj;
}

//! A function that returns the adjacency matrix for a given image size and a weighted
//! neighborhood.
/*!
 * The computation is done via the CPU.
 * \param i_img The image size.
 * \param i_neigh The neighborhood matrix containing the weights for each element.
 * \param i_normalize If true the rows will be normalized to one.
 */
template<typename T>
device_csr_matrix<T> adj_matrix_cpu(const Size& i_img, const device_matrix<T>& i_neigh,
									bool i_normalize)
{
	Expects(i_neigh.rows() < i_img.rows());
	Expects(i_neigh.cols() < i_img.cols());
	Expects(i_neigh.rows() % 2 == 1);
	Expects(i_neigh.cols() % 2 == 1);
	Expects(i_neigh.ld() == i_neigh.rows());

	const auto nnz = ((i_img.rows() - i_neigh.rows() + 1) * i_neigh.rows() +
					  (3 * i_neigh.rows() - 1) * (i_neigh.rows() - 1) / 4) *
					 ((i_img.cols() - i_img.cols() + 1) * i_img.cols() +
					  (3 * i_img.cols() - 1) * (i_img.cols() - 1) / 4);
	const auto px_count = i_img.rows() * i_img.cols();

	auto val = thrust::host_vector<T>(nnz, T{});
	auto col_ind = thrust::host_vector<int>(nnz, 0);
	auto row_ptr = thrust::host_vector<int>(px_count + 1, 0);

	const auto px2 = (i_neigh.rows() - 1) / 2;
	const auto py2 = (i_neigh.cols() - 1) / 2;

	const thrust::host_vector<T> neigh = i_neigh.container();

	auto ind = gsl::index{};
	for(gsl::index i = 0; i < i_img.rows(); i++)
	{
		for(gsl::index j = 0; j < i_img.cols(); j++)
		{
			auto ind_px = gsl::index{};
			const auto begin_k = max(gsl::index{}, i - px2);
			const auto end_k = min(i_img.rows(), i + px2 + 1);

			for(gsl::index k = begin_k; k < end_k; k++)
			{
				const auto begin_l = max(gsl::index{}, j - py2);
				const auto end_l = min(i_img.cols(), j + py2 + 1);

				for(gsl::index l = begin_l; l < end_l; l++)
				{
					const auto neigh_ind = k - i + px2 + i_neigh.cols() * (l - j + py2);
					val[ind + ind_px] = neigh[neigh_ind];
					col_ind[ind + ind_px] = i_img.cols() * k + l;
					ind_px++;
				}

				ind += ind_px;
				row_ptr[i_img.cols() * i + j + 1] = ind;
			}
		}
	}

	Expects(ind == nnz);

	for(gsl::index i = 0; i < row_ptr.size() - 1; i++)
	{
		const auto begin = row_ptr[i];
		const auto end = row_ptr[i + 1];
		auto sum = T{};

		for(gsl::index j = begin; j < end; j++)
			sum += val[j];

		for(gsl::index j = begin; j < end; j++)
			val[j] /= sum;
	}

	return device_csr_matrix<T>{Rows{px_count}, Cols{px_count}, val, row_ptr, col_ind};
}