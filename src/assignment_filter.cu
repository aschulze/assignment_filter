#include "../include/adj_matrix.h"
#include "../include/assignment_filter.h"
#include "../include/assignment_filter_helper.h"
#include "../include/distance_matrix.h"
#include "../include/label_image.h"

std::unique_ptr<assignment_filter> assignment_filter::create(assignment_param i_param,
															 term_criteria i_term)
{
	return std::make_unique<assignment_filter>(std::move(i_param), std::move(i_term),
											   ctor_cookie{});
}

device_matrix<vec3<float>>
	assignment_filter::compute(const device_matrix<vec3<float>>& i_image,
							   const thrust::device_vector<vec3<float>>& i_labels)
{
	const auto d_D = distance_matrix<euclidean_metric>(i_image, i_labels);
	const auto d_W = compute_assignment_prob(d_D, i_image.size());

	return label_image(i_image.size(), d_W, i_labels);
}

device_matrix<float> assignment_filter::compute_assignment_prob(const device_matrix<float>& i_D,
																const Size& i_size_image)
{
	auto o_W = initialize(i_D, i_size_image);
	const auto k = gsl::narrow_cast<float>(i_D.cols());

	// Iterate until the maximum number of iterations is reached or the termination criteria at
	// the end of the scope is satisfied.
	for(size_t i = 0; i < m_term.max_iter; i++)
	{
		// Determine the likelihood matrix.
		// L = W * exp(-U) and normalize the rows.
		likelihood_matrix(o_W, m_U, m_L);

		// Determine the likelihood matrix by lifting the distance matrix to the assignment
		// manifold. S = exp(avr_matrix * log(L)) and normalize.
		similarity_matrix(m_sparse_handle, m_sparse_descr, m_avr_matrix, m_L, m_S);

		// Assignment update w/o lifting the similiraty matrix to the assignment manifold.
		assignment_update(o_W, m_S, m_param.gamma, m_param.use_tangent);

		// Epsilon normalization of the assignment provabilities.
		eps_normalization(o_W, m_term.eps);

		// Compute the average entry/ entropy.
		const auto avr_entry = average_entropy(o_W, m_avr_row, k);

		if(avr_entry < m_term.entropy)
			break;
	}

	const auto status = cudaDeviceSynchronize();
	if(status != cudaSuccess)
		throw thrust::system_error{status, thrust::cuda_category()};

	return o_W;
}

device_matrix<float> assignment_filter::compute_distance_matrix(
	const device_matrix<vec3<float>>& i_image,
	const thrust::device_vector<vec3<float>>& i_labels) const
{
	return distance_matrix<euclidean_metric>(i_image, i_labels);
}

device_csr_matrix<float> assignment_filter::compute_adj_matrix(const Size& i_size_image) const
{
	const auto neigh_matrix = device_matrix<float>{m_param.size_neigh, 1.0f};
	const auto normalize = true;

	return adj_matrix(i_size_image, neigh_matrix, normalize);
}

void assignment_filter::set_assignment_parameter(assignment_param i_param)
{
	if(i_param.size_neigh.cols() != m_param.size_neigh.cols() ||
	   i_param.size_neigh.rows() != m_param.size_neigh.rows())
	{
		m_refresh_avr_matrix = true;
	}

	m_param = std::move(i_param);
}

void assignment_filter::set_termination_criteria(term_criteria i_term)
{
	m_term = std::move(i_term);
}

assignment_param assignment_filter::get_assignment_param() const noexcept
{
	return m_param;
}

term_criteria assignment_filter::get_termination_criteria() const noexcept
{
	return m_term;
}

assignment_filter::assignment_filter(assignment_param i_param, term_criteria i_term, ctor_cookie)
	: m_param{std::move(i_param)}, m_term{std::move(i_term)}
{
}

device_matrix<float> assignment_filter::initialize(const device_matrix<float>& i_D,
												   const Size& i_size_image)
{
	Expects(i_size_image.rows() >= m_param.size_neigh.rows());
	Expects(i_size_image.cols() >= m_param.size_neigh.cols());
	Expects(i_size_image.total() == i_D.rows());

	if(m_L.rows() != i_D.rows() || m_L.cols() != i_D.cols())
	{
		m_avr_matrix = compute_adj_matrix(i_size_image);

		// likelihood matrix
		m_L = device_matrix<float>{Rows{i_D.rows()}, Cols{i_D.cols()}, 0.0f};
		// similarity matrix
		m_S = device_matrix<float>{Rows{i_D.rows()}, Cols{i_D.cols()}, 0.0f};
		// buffer for average row values of the probability matrix
		m_avr_row = thrust::device_vector<float>{i_D.rows(), 0.0f};
	}
	// If the neighborhood size has changed the adjacency matrix needs to be recomputed.
	else if(m_refresh_avr_matrix)
	{
		m_avr_matrix = compute_adj_matrix(i_size_image);
	}
	m_refresh_avr_matrix = false;

	m_U = update_matrix(i_D, m_param.rho);

	// Initialization of the assignment probabilities.
	const auto cols = gsl::narrow_cast<float>(i_D.cols());

	return device_matrix<float>{Rows{i_D.rows()}, Cols{i_D.cols()}, 1.0f / cols};
}

// assignment_filter_dbl

std::unique_ptr<assignment_filter_dbl> assignment_filter_dbl::create(assignment_param i_param,
																	 term_criteria i_term)
{
	return std::make_unique<assignment_filter_dbl>(std::move(i_param), std::move(i_term),
												   ctor_cookie{});
}

device_matrix<vec3<float>>
	assignment_filter_dbl::compute(const device_matrix<vec3<float>>& i_image,
								   const thrust::device_vector<vec3<float>>& i_labels)
{
	const auto d_D = distance_matrix_dbl<euclidean_metric>(i_image, i_labels);
	const auto d_W = compute_assignment_prob(d_D, i_image.size());

	return label_image(i_image.size(), d_W, i_labels);
}

device_matrix<double>
	assignment_filter_dbl::compute_assignment_prob(const device_matrix<double>& i_D,
												   const Size& i_size_image)
{
	auto o_W = initialize(i_D, i_size_image);
	const auto k = gsl::narrow_cast<double>(i_D.cols());

	// Iterate until the maximum number of iterations is reached or the termination criteria at
	// the end of the scope is satisfied.
	for(size_t i = 0; i < m_term.max_iter; i++)
	{
		// Determine the likelihood matrix.
		// L = W * exp(-U) and normalize the rows.
		likelihood_matrix(o_W, m_U, m_L);

		// Determine the likelihood matrix by lifting the distance matrix to the assignment
		// manifold. S = exp(avr_matrix * log(L)) and normalize.
		similarity_matrix(m_sparse_handle, m_sparse_descr, m_avr_matrix, m_L, m_S);

		// Assignment update w/o lifting the similiraty matrix to the assignment manifold.
		assignment_update(o_W, m_S, static_cast<double>(m_param.gamma), m_param.use_tangent);

		// Epsilon normalization of the assignment provabilities.
		eps_normalization(o_W, static_cast<double>(m_term.eps));

		// Compute the average entry/ entropy.
		const auto avr_entry = average_entropy(o_W, m_avr_row, k);

		if(avr_entry < m_term.entropy)
		{
			break;
		}
	}

	const auto status = cudaDeviceSynchronize();
	if(status != cudaSuccess)
		throw thrust::system_error{status, thrust::cuda_category()};

	return o_W;
}

device_matrix<double> assignment_filter_dbl::compute_distance_matrix(
	const device_matrix<vec3<float>>& i_image,
	const thrust::device_vector<vec3<float>>& i_labels) const
{
	return distance_matrix_dbl<euclidean_metric>(i_image, i_labels);
}

device_csr_matrix<double> assignment_filter_dbl::compute_adj_matrix(const Size& i_size_image) const
{
	const auto neigh_matrix = device_matrix<double>{m_param.size_neigh, 1.0f};
	const auto normalize = true;

	return adj_matrix(i_size_image, neigh_matrix, normalize);
}

void assignment_filter_dbl::set_assignment_parameter(assignment_param i_param)
{
	if(i_param.size_neigh.cols() != m_param.size_neigh.cols() ||
	   i_param.size_neigh.rows() != m_param.size_neigh.rows())
	{
		m_refresh_avr_matrix = true;
	}

	m_param = std::move(i_param);
}

void assignment_filter_dbl::set_termination_criteria(term_criteria i_term)
{
	m_term = std::move(i_term);
}

assignment_param assignment_filter_dbl::get_assignment_param() const noexcept
{
	return m_param;
}

term_criteria assignment_filter_dbl::get_termination_criteria() const noexcept
{
	return m_term;
}

assignment_filter_dbl::assignment_filter_dbl(assignment_param i_param, term_criteria i_term,
											 ctor_cookie)
	: m_param{std::move(i_param)}, m_term{std::move(i_term)}
{
}

device_matrix<double> assignment_filter_dbl::initialize(const device_matrix<double>& i_D,
														const Size& i_size_image)
{
	Expects(i_size_image.rows() >= m_param.size_neigh.rows());
	Expects(i_size_image.cols() >= m_param.size_neigh.cols());
	Expects(i_size_image.total() == i_D.rows());

	if(m_L.rows() != i_D.rows() || m_L.cols() != i_D.cols())
	{
		m_avr_matrix = compute_adj_matrix(i_size_image);

		// likelihood matrix
		m_L = device_matrix<double>{Rows{i_D.rows()}, Cols{i_D.cols()}, 0.0f};
		// similarity matrix
		m_S = device_matrix<double>{Rows{i_D.rows()}, Cols{i_D.cols()}, 0.0f};
		// buffer for average row values of the probability matrix
		m_avr_row = thrust::device_vector<double>{i_D.rows(), 0.0f};
	}
	// If the neighborhood size has changed the adjacency matrix needs to be recomputed.
	else if(m_refresh_avr_matrix)
	{
		m_avr_matrix = compute_adj_matrix(i_size_image);
	}
	m_refresh_avr_matrix = false;

	m_U = update_matrix(i_D, static_cast<double>(m_param.rho));

	// Initialization of the assignment probabilities.
	const auto cols = gsl::narrow_cast<double>(i_D.cols());

	return device_matrix<double>{Rows{i_D.rows()}, Cols{i_D.cols()}, 1.0f / cols};
}