cmake_minimum_required(VERSION 3.10 FATAL_ERROR)
project(lib_assignment_filter_cuda LANGUAGES CXX CUDA)

include(CTest)

if(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
  set(cuda_libs cufft cublas cublasLt nppc nppig cudart_static)
  set(warnings "/W3")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU" OR CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(cuda_libs -lcublasLt_static -lcublas_static -lcufft_static_nocallback
                -llapack_static -lculibos -lcudart_static -lpthread -ldl -lcublasLt_static)
                # cublaslLT_static needs to be linked twice
  set(warnings "-Werror -Wall -Wextra -Wduplicated-cond -Wduplicated-branches -Wlogical-op \
              -Wrestrict -Wnull-dereference -Wformat-signedness -Wodr -Wredundant-decls -Wcast-align \
              -Wmissing-include-dirs -Wswitch-enum -Wswitch-default -Wredundant-decls \
              -Wmissing-format-attribute -Wformat-nonliteral -Wconversion -Wpointer-arith \
              -Wsequence-point -Wparentheses")
endif(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")

set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} \
  -Xcudafe \"--diag_suppress=unsigned_compare_with_zero\" \
  -Xcompiler \"${warnings}\" \
  -expt-extended-lambda"
)

include_directories(SYSTEM extern)

#
## static library
#
add_library(lib_assignment_filter STATIC
  include/adj_matrix.h
  include/assignment_filter.h
  include/assignment_filter_helper.h
  include/device_matrix.h
  include/distance_matrix.h
  include/label_image.h
  include/metric.h
  include/strided_range.h
  src/assignment_filter.cu
  src/assignment_filter_helper.cu
  extern/lodepng/lodepng.cpp
)
target_compile_features(lib_assignment_filter PRIVATE cxx_std_14)
set_target_properties(lib_assignment_filter PROPERTIES POSITION_INDEPENDENT_CODE ON)

if(APPLE)
  set_property(TARGET lib_assignment_filter PROPERTY BUILD_RPATH ${CMAKE_CUDA_IMPLICIT_LINK_DIRECTORIES})
endif()

target_include_directories(lib_assignment_filter PUBLIC ${CMAKE_SOURCE_DIR} include)
target_link_libraries(lib_assignment_filter -lcublas_static -lcusparse_static)

set_target_properties(lib_assignment_filter PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
SET_TARGET_PROPERTIES(lib_assignment_filter PROPERTIES LINKER_LANGUAGE CUDA)

#
## small benchmark
#
add_executable(test_assignment_filter
  tests/main.cpp
  tests/test_assignment_filter.cu
  tests/test_assignment_filter.h
  tests/test_helper.h
)

set_target_properties(test_assignment_filter PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
target_link_libraries(test_assignment_filter PRIVATE lib_assignment_filter -lcublas_static
  -lcusparse_static -llapack_static -lculibos -lcudart_static)