#pragma once

#include "../include/device_matrix.h"

#include "../extern/gsl/gsl_util"
#include "../extern/lodepng/lodepng.h"

#include <exception>
#include <string>

using namespace std::string_literals;

//! A template function that returns the underlying data vector in host memory.
/*!
 * \param i_matrix Matrix.
 * \return Underlying data vector in host memory.
 */
template<typename T>
std::vector<T> get_host_vector(const device_matrix<T>& i_matrix)
{
	auto h_vec = thrust::host_vector<T>(i_matrix.cbegin(), i_matrix.cend());

	return std::vector<T>(h_vec.cbegin(), h_vec.cend());
}

//! Projects a pixel value in [0,1] to unsigned char.
/*!
 * \param i_px Float or double pixel value.
 * \return Saturated unsigned char pixel value.
 */
constexpr unsigned char to_uchar(float i_px)
{
	return gsl::narrow_cast<unsigned char>(std::min(std::max(255.0f * i_px, 0.0f), 255.0f));
}

constexpr unsigned char to_uchar(double i_px)
{
	return gsl::narrow_cast<unsigned char>(std::min(std::max(255.0 * i_px, 0.0), 255.0));
}

//! Projects unsigned char to float [0,1].
/*!
 * \param i_px Float or double pixel value.
 * \return Float in [0,1].
 */
constexpr float to_T(unsigned char i_px, float /*tag*/) noexcept
{
	return gsl::narrow_cast<float>(i_px) / 255.0f;
}

constexpr double to_T(unsigned char i_px, double /*tag*/) noexcept
{
	return gsl::narrow_cast<double>(i_px) / 255.0;
}

template<typename T>
std::vector<T> to_col_major(const std::vector<T>& i_data, Rows rows, Cols cols)
{
	auto out = std::vector<T>(i_data.size());
	for(size_t i = 0; i < i_data.size(); i++)
	{
		const auto col = i % cols.value();
		const auto row = i / cols.value();
		const auto idx = row + col * rows.value();
		out.at(idx) = i_data.at(i);
	}

	return out;
}

template<typename T>
std::vector<T> to_row_major(const std::vector<T>& i_data, Rows rows, Cols cols)
{
	auto out = std::vector<T>(i_data.size());
	for(size_t i = 0; i < i_data.size(); i++)
	{
		const auto row = i % rows.value();
		const auto col = i / rows.value();
		const auto idx = col + row * cols.value();
		out.at(idx) = i_data.at(i);
	}

	return out;
}

template<typename T>
device_matrix<vec3<T>> load_image_rgb_impl(const std::string& filename, vec3<T> /*tag*/)
{
	auto png = std::vector<unsigned char>{};
	auto raw = std::vector<unsigned char>{};
	auto width = uint{};
	auto height = uint{};

	auto error = lodepng::load_file(png, filename);
	if(!error)
	{
		error = lodepng::decode(raw, width, height, png, LodePNGColorType::LCT_RGB);
	}
	if(error)
	{
		throw std::runtime_error{"Error loading image \""s + filename + "\".\n" +
								 lodepng_error_text(error)};
	}

	// Remark: The fourth/ alpha channel has no information.
	auto rgb = [&raw, width, height] {
		auto tmp = std::vector<vec3<T>>(raw.size() / 3);
		for(size_t i = 0; i < tmp.size(); i++)
		{
			const auto r = to_T(raw.at(i * 3), T{});
			const auto g = to_T(raw.at(i * 3 + 1), T{});
			const auto b = to_T(raw.at(i * 3 + 2), T{});
			tmp.at(i) = vec3<T>{r, g, b};
		}

		return to_col_major(tmp, Rows{height}, Cols{width});
	}();

	return device_matrix<vec3<T>>{rgb, Rows{height}, Cols{width}};
}

device_matrix<vec3<float>> load_image(const std::string& filename, vec3<float> tag)
{
	return load_image_rgb_impl(filename, tag);
}

device_matrix<vec3<double>> load_image(const std::string& filename, vec3<double> tag)
{
	return load_image_rgb_impl(filename, tag);
}

void store_image_impl(const std::string& filename, const device_matrix<vec3<float>>& image)
{
	const auto image_rm =
		to_row_major(get_host_vector(image), Rows{image.rows()}, Cols{image.cols()});
	auto rgb = std::vector<unsigned char>{};
	rgb.reserve(image_rm.size() * 3);
	for(const auto& px : image_rm)
	{
		rgb.push_back(to_uchar(px._1));
		rgb.push_back(to_uchar(px._2));
		rgb.push_back(to_uchar(px._3));
	}

	const auto cols = gsl::narrow<unsigned int>(image.cols());
	const auto rows = gsl::narrow<unsigned int>(image.rows());
	auto error = lodepng::encode(filename, rgb, cols, rows, LodePNGColorType::LCT_RGB);
	if(error)
	{
		throw std::runtime_error{"Error storing image. \n"s + lodepng_error_text(error)};
	}
}

void store_image_impl(const std::string& filename, const device_matrix<vec3<double>>& image)
{
	const auto image_rm =
		to_row_major(get_host_vector(image), Rows{image.rows()}, Cols{image.cols()});
	auto rgb = std::vector<unsigned char>{};
	rgb.reserve(image_rm.size() * 3);
	for(const auto& px : image_rm)
	{
		rgb.push_back(to_uchar(px._1));
		rgb.push_back(to_uchar(px._2));
		rgb.push_back(to_uchar(px._3));
	}

	const auto cols = gsl::narrow<unsigned int>(image.cols());
	const auto rows = gsl::narrow<unsigned int>(image.rows());
	auto error = lodepng::encode(filename, rgb, cols, rows, LodePNGColorType::LCT_RGB);
	if(error)
	{
		throw std::runtime_error{"Error storing image. \n"s + lodepng_error_text(error)};
	}
}

void store_image(const std::string& filename, const device_matrix<vec3<float>>& image)
{
	store_image_impl(filename + ".png", image);
}

void store_image(const std::string& filename, const device_matrix<vec3<double>>& image)
{
	store_image_impl(filename + ".png", image);
}