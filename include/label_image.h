#pragma once

#include "device_matrix.h"

#include "../extern/gsl/gsl_assert"

#include <thrust/device_vector.h>

#include <cuda_runtime.h> // CUDA C++ API
#include <device_launch_parameters.h>

//! A CUDA kernel that finds the column index of the maximal value in each row of the first array.
//! The element of the second array at that index will be written to the third array at position
//! row.
template<typename T>
__global__ void label_image_kernel(const float* dev_a, unsigned int dev_a_rows, const T* dev_b,
								   unsigned int dev_b_size, T* dev_c)
{
	// Get the id of a thread within a block.
	auto tid = blockIdx.x * blockDim.x + threadIdx.x;

	if(tid < dev_a_rows)
	{
		// Each thread finds the maximal value of a row of dev_a.
		auto max_val = float{};
		for(unsigned int i = 0; i < dev_b_size; i++)
		{
			max_val = fmaxf(max_val, dev_a[tid + i * dev_a_rows]);
		}

		// Each threads writes the value of dev_b at the index of max_val to dev_c.
		for(unsigned int i = 0; i < dev_b_size; i++)
		{
			if(max_val == dev_a[tid + i * dev_a_rows])
			{
				dev_c[tid] = dev_b[i];
				break;
			}
		}
	}
}

//! A CUDA kernel that finds the column index of the maximal value in each row of the first array.
//! The element of the second array at that index will be written to the third array at position
//! row.
template<typename T>
__global__ void label_image_kernel(const double* dev_a, unsigned int dev_a_rows, const T* dev_b,
								   unsigned int dev_b_size, T* dev_c)
{
	// Get the id of a thread within a block.
	auto tid = blockIdx.x * blockDim.x + threadIdx.x;

	if(tid < dev_a_rows)
	{
		// Each thread finds the maximal value of a row of dev_a.
		auto max_val = double{};
		for(unsigned int i = 0; i < dev_b_size; i++)
		{
			max_val = fmax(max_val, dev_a[tid + i * dev_a_rows]);
		}

		// Each threads writes the value of dev_b at the index of max_val to dev_c.
		for(unsigned int i = 0; i < dev_b_size; i++)
		{
			if(max_val == dev_a[tid + i * dev_a_rows])
			{
				dev_c[tid] = dev_b[i];
				break;
			}
		}
	}
}

//! A function that generates an image based on assignment probabilities and labels.
/*!
 * The number of columns of the assignment probability matrix has to be equal to the number of
 * labels. The number of rows of the probability matrix has to be equal to the total size of the
 * output image.
 * \param i_W The assignment probabilities of the image pixels. It is assumed to be in column-major
 * format.
 * \param i_labels The labels to assign to the pixels.
 * \return The output image. This matrix is in row-major format if the pixels in the assignment
 * probabilities matrix are stored in row-major format. It is in column-major format otherwise.
 */
template<typename T, typename U>
device_matrix<T> label_image(Size i_image_size, const device_matrix<U>& i_W,
							 const thrust::device_vector<T>& i_labels)
{
	Expects(i_image_size.total() == i_W.rows());
	Expects(i_W.cols() == i_labels.size());

	auto o_image = device_matrix<T>{Rows{i_image_size.rows()}, Cols{i_image_size.cols()}};

	const auto rows_W = gsl::narrow<unsigned int>(i_W.rows());
	const auto size_labels = gsl::narrow<unsigned int>(i_labels.size());
	const auto block = dim3{512};
	const auto grid = ((rows_W * size_labels + block.x - 1) / block.x);

	label_image_kernel<<<grid, block>>>(i_W.data().get(), rows_W, i_labels.data().get(),
										size_labels, o_image.data().get());

	// Check if the kernel execution generated an error.
	const auto status = cudaDeviceSynchronize();
	if(status != cudaSuccess)
		throw thrust::system_error{status, thrust::cuda_category()};

	return o_image;
}