#pragma once

#include "device_matrix.h"

#include <cusparse_v2.h>

#include <thrust/for_each.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/system_error.h>

#include "../extern/gsl/gsl_assert"

//! A class that wraps the creation of a cuda stream.
class unique_cuda_stream final
{
  public:
	unique_cuda_stream()
	{
		const auto status = cudaStreamCreate(&m_stream);
		if(status != cudaSuccess)
			throw thrust::system_error{status, thrust::cuda_category()};
	}

	~unique_cuda_stream() noexcept
	{
		cudaStreamDestroy(m_stream);
	}

	cudaStream_t get() const noexcept
	{
		return m_stream;
	}

  private:
	cudaStream_t m_stream{};
};

//! A class that wraps the construction and life time of a cusparse handle.
class unique_cusparse_handle final
{
  public:
	unique_cusparse_handle()
	{
		const auto status = cusparseCreate(&m_handle);

		if(status != CUSPARSE_STATUS_SUCCESS)
			throw thrust::system_error{status, thrust::cuda_category()};
	}

	unique_cusparse_handle(unique_cusparse_handle&&) = default;
	unique_cusparse_handle& operator=(unique_cusparse_handle&&) = default;

	~unique_cusparse_handle()
	{
		if(m_handle)
			cusparseDestroy(m_handle);
	}

	constexpr cusparseHandle_t get() const noexcept
	{
		return m_handle;
	}

  private:
	cusparseHandle_t m_handle{};
};

//! A class that wraps the construction and life time of a cusparse descriptor.
class unique_cusparse_descriptor final
{
  public:
	unique_cusparse_descriptor()
	{
		const auto status = cusparseCreateMatDescr(&m_handle);

		if(status != CUSPARSE_STATUS_SUCCESS)
			throw thrust::system_error{status, thrust::cuda_category()};

		cusparseSetMatType(m_handle, CUSPARSE_MATRIX_TYPE_GENERAL);
		cusparseSetMatIndexBase(m_handle, CUSPARSE_INDEX_BASE_ZERO);
		cusparseSetMatDiagType(m_handle, CUSPARSE_DIAG_TYPE_NON_UNIT);
	}

	unique_cusparse_descriptor(unique_cusparse_descriptor&&) = default;
	unique_cusparse_descriptor& operator=(unique_cusparse_descriptor&&) = default;

	~unique_cusparse_descriptor()
	{
		if(m_handle)
			cusparseDestroyMatDescr(m_handle);
	}

	constexpr cusparseMatDescr_t get() const noexcept
	{
		return m_handle;
	}

  private:
	cusparseMatDescr_t m_handle{};
};

//! B = Unary(A)
template<template<class> typename Unary, typename T>
void element_wise(device_matrix<T>& i_A)
{
	thrust::for_each(i_A.begin(), i_A.end(),
					 [=] __device__(auto& val) { val = Unary<T>::compute(val); });
}

//! Template base class for the natural logarithm.
template<typename T>
struct Log
{
};

//! Concrete template class for the natural logarithm.
template<>
struct Log<float>
{
	__host__ __device__ static float compute(float x)
	{
		return logf(x);
	}
};

//! Concrete template class for the natural logarithm.
template<>
struct Log<double>
{
	__host__ __device__ static double compute(double x)
	{
		return log(x);
	}
};

//! Overload for matrix multiply add.
//! C = alpha * A * B + beta * C, where A is sparse and B, C dense
void mad(cusparseHandle_t i_cusparse_handle, cusparseMatDescr_t i_descr, float i_alpha,
		 const device_csr_matrix<float>& i_A, const device_matrix<float>& i_B, float i_beta,
		 device_matrix<float>& i_C);

//! Overload for matrix multiply add.
//! C = alpha * A * B + beta * C, where A is sparse and B, C dense
void mad(cusparseHandle_t i_cusparse_handle, cusparseMatDescr_t i_descr, double i_alpha,
		 const device_csr_matrix<double>& i_A, const device_matrix<double>& i_B, double i_beta,
		 device_matrix<double>& i_C);

//! TODO
template<typename T>
device_matrix<T> update_matrix(const device_matrix<T>& i_D, T i_rho)
{
	Expects(i_rho != T{0});

	// U = (D - row_wise_sum(D)) / rho = (D*one_matrix/(-rho) + D/rho)
	auto o_U = i_D;

	const auto rows = i_D.rows();
	const auto cols = i_D.cols();

	// Compute the inverse to avoid slow floating point division later.
	const auto inv_cols = 1.0f / gsl::narrow_cast<float>(cols);
	const auto inv_rho = 1.0f / i_rho;

	auto o_U_ptr = o_U.data().get();
	const auto i_D_ptr = i_D.data().get();

	const auto first = thrust::counting_iterator<std::size_t>{0};
	const auto last = first + i_D.rows();

	thrust::for_each(first, last, [=] __device__(std::size_t t_row) {
		auto mean = T{};
		for(std::size_t i = 0; i < cols; i++)
		{
			mean += i_D_ptr[t_row + i * rows];
		}
		mean *= inv_cols;

		for(std::size_t i = 0; i < cols; i++)
		{
			const auto index = t_row + i * rows;

			o_U_ptr[index] = (i_D_ptr[index] - mean) * inv_rho;
		}
	});

	return o_U;
}

//! L = W * exp(-U) and normalize the rows.
template<typename T>
void likelihood_matrix(const device_matrix<T>& i_W, const device_matrix<T>& i_U,
					   device_matrix<T>& o_L)
{
	Expects(i_W.rows() == i_U.rows());
	Expects(i_W.cols() == i_U.cols());
	Expects(i_W.rows() == o_L.rows());
	Expects(i_W.cols() == o_L.cols());

	const auto rows = i_W.rows();
	const auto cols = i_W.cols();

	auto o_L_ptr = o_L.data().get();
	const auto i_W_ptr = i_W.data().get();
	const auto i_U_ptr = i_U.data().get();

	const auto first = thrust::counting_iterator<std::size_t>{0};
	const auto last = first + i_W.rows();

	thrust::for_each(first, last, [=] __device__(std::size_t t_row) {
		auto sum = T{};
		for(std::size_t i = 0; i < cols; i++)
		{
			const auto index = t_row + i * rows;
			const auto val = i_W_ptr[index] * exp(T{-1} * i_U_ptr[index]);

			o_L_ptr[index] = val;
			sum += val;
		}
		// Compute the inverse to avoid slow floating point division later.
		const auto inv_sum = T{1} / sum;

		for(std::size_t i = 0; i < cols; i++)
		{
			const auto index = t_row + i * rows;
			o_L_ptr[index] *= inv_sum;
		}
	});
}

//! S = exp(avr_matrix * log(L)) and normalize.
template<typename T>
void similarity_matrix(const unique_cusparse_handle& i_sparse_handle,
					   const unique_cusparse_descriptor& i_sparse_descr,
					   const device_csr_matrix<T>& i_avr, device_matrix<T>& o_L,
					   device_matrix<T>& o_S)
{
	Expects(i_avr.rows() == i_avr.cols());
	Expects(i_avr.rows() == o_L.rows());
	Expects(o_L.rows() == o_S.rows());
	Expects(o_L.cols() == o_S.cols());

	// L = log(L)
	element_wise<Log>(o_L);

	// S = avr_matrix * log(L)
	mad(i_sparse_handle.get(), i_sparse_descr.get(), 1.0f, i_avr, o_L, 0.0f, o_S);

	const auto rows = o_L.rows();
	const auto cols = o_L.cols();

	auto o_S_ptr = o_S.data().get();

	const auto first = thrust::counting_iterator<std::size_t>{0};
	const auto last = first + o_L.rows();

	// S = exp(S) with S = avr_matrix * log(L)
	// S /= row_wise_sum(S)
	thrust::for_each(first, last, [=] __device__(std::size_t t_row) {
		auto sum = T{};
		for(std::size_t i = 0; i < cols; i++)
		{
			const auto index = t_row + i * rows;
			const auto val = exp(o_S_ptr[index]);

			o_S_ptr[index] = val;
			sum += val;
		}
		// Compute the inverse to avoid slow floating point division later.
		const auto inv_sum = T{1} / sum;

		for(std::size_t i = 0; i < cols; i++)
		{
			const auto index = t_row + i * rows;
			o_S_ptr[index] *= inv_sum;
		}
	});
}

//! TODO
template<typename T>
void assignment_update(device_matrix<T>& o_W, device_matrix<T>& o_S, T i_gamma,
					   bool i_use_tangent_approach)
{
	Expects(o_W.rows() == o_S.rows());
	Expects(o_W.cols() == o_S.cols());

	const auto rows = o_W.rows();
	const auto cols = o_W.cols();

	auto o_W_ptr = o_W.data().get();
	const auto o_S_ptr = o_S.data().get();

	if(i_use_tangent_approach)
	{
		const auto first = thrust::counting_iterator<std::size_t>{0};
		const auto last = first + o_W.rows();

		thrust::for_each(first, last, [=] __device__(std::size_t t_row) {
			auto mean = T{};
			for(std::size_t i = 0; i < cols; i++)
			{
				mean += o_S_ptr[t_row + i * rows];
			}
			mean /= cols;

			for(std::size_t i = 0; i < cols; i++)
			{
				const auto index = t_row + i * rows;

				const auto val = o_S_ptr[index] - mean;

				o_S_ptr[index] = val;
				o_W_ptr[index] = o_W_ptr[index] * exp(i_gamma * val);
			}
		});
	}
	else
	{
		const auto first = thrust::counting_iterator<std::size_t>{0};
		const auto last = first + o_W.total();

		// W = W^gamma * S
		thrust::for_each(first, last, [=] __device__(std::size_t index) {
			o_W_ptr[index] = pow(o_W_ptr[index], i_gamma) * o_S_ptr[index];
		});
	}
}

template<typename T>
void eps_normalization(device_matrix<T>& o_W, T i_eps)
{
	const auto rows = o_W.rows();
	const auto cols = o_W.cols();

	auto o_W_ptr = o_W.data().get();

	const auto first = thrust::counting_iterator<std::size_t>{0};
	const auto last = first + o_W.rows();

	thrust::for_each(first, last, [=] __device__(std::size_t t_row) {
		auto pre_eps_sum = T{};
		for(std::size_t i = 0; i < cols; i++)
		{
			pre_eps_sum += o_W_ptr[t_row + i * rows];
		}
		const auto inv_pre_eps_sum = T{1} / pre_eps_sum;

		auto post_eps_sum = T{};
		for(std::size_t i = 0; i < cols; i++)
		{
			const auto index = t_row + i * rows;
			const auto val = max(i_eps, o_W_ptr[index] * inv_pre_eps_sum);

			o_W_ptr[index] = val;
			post_eps_sum += val;
		}
		// Compute the inverse to avoid slow floating point division later.
		const auto inv_post_eps_sum = T{1} / post_eps_sum;

		for(std::size_t i = 0; i < cols; i++)
		{
			const auto index = t_row + i * rows;
			o_W_ptr[index] *= inv_post_eps_sum;
		}
	});
}

//! Compute the average entry/ entropy.
//! avr_entr = -row_wise_sum(W * log(W)) / log(K)
template<typename T>
T average_entropy(const device_matrix<T>& i_W, thrust::device_vector<T>& o_avr_rows, T i_K)
{
	Expects(i_W.rows() == o_avr_rows.size());

	const auto rows = i_W.rows();
	const auto cols = i_W.cols();

	const auto i_W_ptr = i_W.data().get();
	auto o_avr_rows_ptr = o_avr_rows.data().get();

	const auto first = thrust::counting_iterator<std::size_t>{0};
	const auto last = first + i_W.rows();

	// o_avr_rows = row_wise_sum(W * log(W))
	thrust::for_each(first, last, [=] __device__(std::size_t t_row) {
		auto sum = T{};
		for(std::size_t i = 0; i < cols; i++)
		{
			const auto index = t_row + i * rows;
			sum += i_W_ptr[index] * log(i_W_ptr[index]);
		}

		o_avr_rows_ptr[t_row] = sum;
	});

	const auto o_avr_row = T{-1} / gsl::narrow_cast<T>(rows) / gsl::narrow_cast<T>(log(i_K)) *
						   thrust::reduce(o_avr_rows.cbegin(), o_avr_rows.cend());

	return o_avr_row;
}