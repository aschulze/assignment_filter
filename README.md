# IMAGE LABELING BY ASSIGNMENT

CUDA implementation of the the Assignment Flow by Åström, et al [1].

![Epochen](tests/images/mandrill.png)
![Epochen](tests/results/mandrill_labels_10_7x7.png)

Figure 1: Left: Original image. Right: Assignment flow with 10 labels (chosen via k-means
clustering) and a neighborhood of size 7x7.

\[1\] Åström, Freddie et al. “Image Labeling by Assignment.” Journal of Mathematical Imaging and Vision 58.2 (2017): 211–238. Crossref. Web.

## Prerequisites

An NVIDIA graphics card of compute capability 3.x or higher, CUDA 10.0 and CMake 3.10.

## C++ Static Library

```
	mkdir build
	cd build
	cmake ..
	make lib_assignment_filter
```

### Generating Test Images

```
	mkdir build
	cd build
	cmake ..
	make test_assignment_filter
    cd ..
	./build/test_assignment_filter
```

## Built With

* [ms-gsl](https://github.com/microsoft/GSL) - Guidelines Support Library
* [LodePNG](https://github.com/lvandeve/lodepng) - PNG encoder and decoder in C and C++, without dependencies

## Authors

* **André Schulze**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.