#pragma once

#include "device_matrix.h"

#ifdef __CUDACC__
#define CUDA_HOSTDEV __host__ __device__
#else
#define CUDA_HOSTDEV
#endif

//! Class that implements the Euclidean metric.
struct euclidean_metric final
{
	//! A static member that computes the Euclidean distance between two sets of points.
	/*!
	 * \param lhs First set of points.
	 * \param rhs Second set of points.
	 * \param i_length Number of input elements of lhs and rhs.
	 * \return The euclidean distance for the two input sets.
	 */
	CUDA_HOSTDEV static float compute(const float lhs, const float rhs) noexcept
	{
		return abs(lhs - rhs);
	}

	/*!
	 * \overload
	 */
	CUDA_HOSTDEV static double compute(const double lhs, const double rhs) noexcept
	{
		return abs(lhs - rhs);
	}

	/*!
	 * \overload
	 */
	CUDA_HOSTDEV static float compute(const vec3<float>& lhs, const vec3<float>& rhs) noexcept
	{
		const auto x = lhs._1 - rhs._1;
		const auto y = lhs._2 - rhs._2;
		const auto z = lhs._3 - rhs._3;

		return sqrt(x * x + y * y + z * z);
	}

	/*!
	 * \overload
	 */
	CUDA_HOSTDEV static double compute(const vec3<double>& lhs, const vec3<double>& rhs) noexcept
	{
		const auto x = lhs._1 - rhs._1;
		const auto y = lhs._2 - rhs._2;
		const auto z = lhs._3 - rhs._3;

		return sqrt(x * x + y * y + z * z);
	}
};