#include "../include/assignment_filter_helper.h"

void mad(cusparseHandle_t i_cusparse_handle, cusparseMatDescr_t i_descr, float i_alpha,
    const device_csr_matrix<float>& i_A, const device_matrix<float>& i_B, float i_beta,
    device_matrix<float>& i_C)
{
    Expects(i_A.cols() == i_B.rows());
    Expects(i_A.rows() == i_C.rows());
    Expects(i_B.cols() == i_C.cols());

    const auto m = gsl::narrow<int>(i_A.rows());
    const auto n = gsl::narrow<int>(i_B.cols());
    const auto k = gsl::narrow<int>(i_A.cols());
    const auto nnz = gsl::narrow<int>(i_A.nnz());
    const auto ldb = gsl::narrow<int>(i_B.ld());
    const auto ldc = gsl::narrow<int>(i_C.ld());

    const auto status = cusparseScsrmm(i_cusparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE, m, n, k,
                                    nnz, &i_alpha, i_descr, i_A.value_data().get(),
                                    i_A.row_ptr_data().get(), i_A.col_ind_data().get(),
                                    i_B.data().get(), ldb, &i_beta, i_C.data().get(), ldc);

    if(status != CUSPARSE_STATUS_SUCCESS)
        throw thrust::system_error{status, thrust::cuda_category()};
}

void mad(cusparseHandle_t i_cusparse_handle, cusparseMatDescr_t i_descr, double i_alpha,
const device_csr_matrix<double>& i_A, const device_matrix<double>& i_B, double i_beta,
device_matrix<double>& i_C)
{
    Expects(i_A.cols() == i_B.rows());
    Expects(i_A.rows() == i_C.rows());
    Expects(i_B.cols() == i_C.cols());

    const auto m = gsl::narrow<int>(i_A.rows());
    const auto n = gsl::narrow<int>(i_B.cols());
    const auto k = gsl::narrow<int>(i_A.cols());
    const auto nnz = gsl::narrow<int>(i_A.nnz());
    const auto ldb = gsl::narrow<int>(i_B.ld());
    const auto ldc = gsl::narrow<int>(i_C.ld());

    const auto status = cusparseDcsrmm(i_cusparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE, m, n, k,
                                nnz, &i_alpha, i_descr, i_A.value_data().get(),
                                i_A.row_ptr_data().get(), i_A.col_ind_data().get(),
                                i_B.data().get(), ldb, &i_beta, i_C.data().get(), ldc);

    if(status != CUSPARSE_STATUS_SUCCESS)
        throw thrust::system_error{status, thrust::cuda_category()};
}