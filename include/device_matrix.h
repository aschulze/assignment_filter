#pragma once

#include "strided_range.h"

#include "../extern/gsl/gsl_assert"
#include "../extern/gsl/gsl_util"

#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/execution_policy.h>
#include <thrust/host_vector.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/iterator_traits.h>

#include <cstddef>
#include <ostream>

//! A type safe class for an offset in row direction.
class RowOffset final
{
  public:
	//! Default constructor.
	constexpr RowOffset() = default;

	//! Constructor.
	/*!
	 * \param i_rows The offset in row direction.
	 */
	constexpr explicit RowOffset(std::size_t i_rows) noexcept : m_row_offset{i_rows} {}
	//! Constructor.
	/*!
	 * \param i_rows The offset in row direction.
	 */
	explicit RowOffset(int i_rows) : m_row_offset{gsl::narrow<std::size_t>(i_rows)} {}

	__host__ __device__ constexpr std::size_t value() const noexcept
	{
		return m_row_offset;
	}

	__host__ __device__ constexpr std::size_t& value() noexcept
	{
		return m_row_offset;
	}

  private:
	std::size_t m_row_offset{};
};

//! A type safe class for an offset in column direction.
class ColOffset final
{
  public:
	constexpr ColOffset() = default;

	constexpr explicit ColOffset(std::size_t i_cols) noexcept : m_col_offset{i_cols} {}
	explicit ColOffset(int i_cols) : m_col_offset{gsl::narrow<std::size_t>(i_cols)} {}

	__host__ __device__ constexpr std::size_t value() const noexcept
	{
		return m_col_offset;
	}

	__host__ __device__ constexpr std::size_t& value() noexcept
	{
		return m_col_offset;
	}

  private:
	std::size_t m_col_offset{};
};

//! A type safe class for rows.
class Rows final
{
  public:
	constexpr Rows() = default;

	constexpr explicit Rows(std::size_t i_rows) noexcept : m_rows{i_rows} {}
	constexpr explicit Rows(unsigned int i_rows) noexcept : m_rows{i_rows} {}
	explicit Rows(int i_rows) : m_rows{gsl::narrow<std::size_t>(i_rows)} {}

	__host__ __device__ constexpr std::size_t value() const noexcept
	{
		return m_rows;
	}

	__host__ __device__ constexpr std::size_t& value() noexcept
	{
		return m_rows;
	}

  private:
	std::size_t m_rows{};
};

//! A type safe class for columns.
class Cols final
{
  public:
	constexpr Cols() = default;

	constexpr explicit Cols(std::size_t i_cols) noexcept : m_cols{i_cols} {}
	constexpr explicit Cols(unsigned int i_cols) noexcept : m_cols{i_cols} {}
	explicit Cols(int i_cols) : m_cols{gsl::narrow<std::size_t>(i_cols)} {}

	__host__ __device__ constexpr std::size_t value() const noexcept
	{
		return m_cols;
	}

	__host__ __device__ constexpr std::size_t& value() noexcept
	{
		return m_cols;
	}

  private:
	std::size_t m_cols{};
};

//! A type safe class for a 2-dimensional size.
class Size final
{
  public:
	constexpr Size() = default;

	constexpr explicit Size(Rows i_rows, Cols i_cols) noexcept
		: m_rows{i_rows.value()}, m_cols{i_cols.value()}
	{
	}

	__host__ __device__ constexpr std::size_t rows() const noexcept
	{
		return m_rows;
	}

	__host__ __device__ constexpr std::size_t& rows() noexcept
	{
		return m_rows;
	}

	__host__ __device__ constexpr std::size_t cols() const noexcept
	{
		return m_cols;
	}

	__host__ __device__ constexpr std::size_t& cols() noexcept
	{
		return m_cols;
	}

	__host__ __device__ constexpr std::size_t total() const noexcept
	{
		return m_rows * m_cols;
	}

  private:
	std::size_t m_rows{};
	std::size_t m_cols{};
};

//! A type safe class for a 2-dimensional size.
class ROI final
{
  public:
	constexpr ROI() = default;

	constexpr explicit ROI(RowOffset i_row_offset, ColOffset i_col_offset, Rows i_rows,
						   Cols i_cols) noexcept
		: m_row_offset{i_row_offset.value()},
		  m_col_offset{i_col_offset.value()}, m_rows{i_rows.value()}, m_cols{i_cols.value()}
	{
	}

	constexpr explicit ROI(RowOffset i_row_offset, ColOffset i_col_offset, Size i_size) noexcept
		: m_row_offset{i_row_offset.value()},
		  m_col_offset{i_col_offset.value()}, m_rows{i_size.rows()}, m_cols{i_size.cols()}
	{
	}

	__host__ __device__ constexpr std::size_t row_offset() const noexcept
	{
		return m_row_offset;
	}

	__host__ __device__ constexpr std::size_t& row_offset() noexcept
	{
		return m_row_offset;
	}

	__host__ __device__ constexpr std::size_t col_offset() const noexcept
	{
		return m_col_offset;
	}

	__host__ __device__ constexpr std::size_t& col_offset() noexcept
	{
		return m_col_offset;
	}

	__host__ __device__ constexpr std::size_t rows() const noexcept
	{
		return m_rows;
	}

	__host__ __device__ constexpr std::size_t& rows() noexcept
	{
		return m_rows;
	}

	__host__ __device__ constexpr std::size_t cols() const noexcept
	{
		return m_cols;
	}

	__host__ __device__ constexpr std::size_t& cols() noexcept
	{
		return m_cols;
	}

	__host__ __device__ constexpr std::size_t total() const noexcept
	{
		return m_rows * m_cols;
	}

  private:
	std::size_t m_row_offset{};
	std::size_t m_col_offset{};
	std::size_t m_rows{};
	std::size_t m_cols{};
};

//! A template struct for a vector of size 3.
template<typename T>
struct vec3 final
{
	T _1{};
	T _2{};
	T _3{};

	__host__ __device__ constexpr void operator+=(const vec3<T> n) noexcept
	{
		_1 += n._1;
		_2 += n._2;
		_3 += n._3;
	}

	__host__ __device__ constexpr void operator-=(const vec3<T> n) noexcept
	{
		_1 -= n._1;
		_2 -= n._2;
		_3 -= n._3;
	}

	__host__ __device__ constexpr void operator*=(float n) noexcept
	{
		_1 *= n;
		_2 *= n;
		_3 *= n;
	}

	__host__ __device__ constexpr void operator/=(float n) noexcept
	{
		_1 /= n;
		_2 /= n;
		_3 /= n;
	}
};

template<typename T>
std::ostream& operator<<(std::ostream& out, const vec3<T>& val)
{
	out << "(" << val._1 << "," << val._2 << "," << val._3 << ")" << std::flush;

	return out;
}

//! A template class for csr-type sparse matrices stored in gpu memory.
//! https://docs.nvidia.com/cuda/cusparse/index.html#compressed-sparse-row-format-csr
template<typename T>
class device_csr_matrix final
{
  public:
	using pointer_t = typename thrust::device_vector<T>::pointer;
	using pointer_int = typename thrust::device_vector<int>::pointer;
	using const_pointer_t = typename thrust::device_vector<T>::const_pointer;
	using const_pointer_int = typename thrust::device_vector<int>::const_pointer;

	//! Default constructor.
	device_csr_matrix() = default;

	//! Constructor.
	/*!
	 * \param i_rows Rows of the matrix.
	 * \param i_cols Columns of the matrix.
	 * \param i_nnz Number of non-zero entries in the matrix.
	 */
	explicit device_csr_matrix(Rows i_rows, Cols i_cols, std::size_t i_nnz)
		: m_csr_val_a(i_nnz), m_csr_row_ptr_a(i_rows.value() + 1),
		  m_csr_col_ind_a(i_nnz), m_rows{i_rows.value()}, m_cols{i_cols.value()}
	{
		Expects(i_nnz > 0);
		Expects(i_nnz <= i_rows.value() * i_cols.value());
	}

	//! Constructor.
	/*!
	 * \param i_rows Rows of the matrix.
	 * \param i_cols Columns of the matrix.
	 * \param i_val The vector containing the non zero elements of the csr matrix.
	 * \param i_row_ptr The vector containing the index to the first non zero element in
	 * each row.
	 * \param i_col_ind The vector containing the column index of each element.
	 */
	explicit device_csr_matrix(Rows i_rows, Cols i_cols, const thrust::host_vector<T>& i_val,
							   const thrust::host_vector<int>& i_row_ptr,
							   const thrust::host_vector<int>& i_col_ind)
		: m_csr_val_a{i_val}, m_csr_row_ptr_a{i_row_ptr},
		  m_csr_col_ind_a{i_col_ind}, m_rows{i_rows.value()}, m_cols{i_cols.value()}
	{
		Expects(i_val.size() == i_col_ind.size());
		Expects(i_row_ptr.size() - 1 == i_rows.value());
		Expects(i_rows.value() * i_cols.value() <= i_val.size());
	}

	//! Move constructor.
	constexpr device_csr_matrix(device_csr_matrix&&) = default;

	//! Move operator.
	device_csr_matrix& operator=(device_csr_matrix&&) = default;

	//! Copy constructor.
	device_csr_matrix(const device_csr_matrix&) = default;

	//! Copy operator.
	device_csr_matrix& operator=(const device_csr_matrix&) = default;

	//! A member returning the number of rows.
	inline constexpr std::size_t rows() const noexcept
	{
		return m_rows;
	}

	//! A member returning the number of columns.
	inline constexpr std::size_t cols() const noexcept
	{
		return m_cols;
	}

	//! A member returning the size of the matrix.
	inline constexpr Size size() const noexcept
	{
		return Size{Rows{m_rows}, Cols{m_cols}};
	}

	//! A member returning the number of non zero elements.
	inline std::size_t nnz() const noexcept
	{
		return m_csr_val_a.size();
	}

	//! A member returning a device_ptr to the first element of the non zero values.
	inline pointer_t value_data() noexcept
	{
		return m_csr_val_a.data();
	}

	//! A member returning a const device_ptr to the first element of the row pointers.
	inline pointer_int row_ptr_data() noexcept
	{
		return m_csr_row_ptr_a.data();
	}

	//! A member returning a const device_ptr to the first element of the column indices.
	inline pointer_int col_ind_data() noexcept
	{
		return m_csr_col_ind_a.data();
	}

	//! A member returning a const device_ptr to the first element of the non zero values.
	inline const_pointer_t value_data() const noexcept
	{
		return m_csr_val_a.data();
	}

	//! A member returning a const device_ptr to the first element of the row pointers.
	inline const_pointer_int row_ptr_data() const noexcept
	{
		return m_csr_row_ptr_a.data();
	}

	//! A member returning a const device_ptr to the first element of the column indices.
	inline const_pointer_int col_ind_data() const noexcept
	{
		return m_csr_col_ind_a.data();
	}

	//! A member returning an const rvalue reference to the non zero matrix values.
	inline thrust::device_vector<T> value() const&
	{
		return m_csr_val_a;
	}

	//! A member returning an const rvalue reference to the row pointers.
	inline thrust::device_vector<int> row_ptr() const&
	{
		return m_csr_row_ptr_a;
	}

	//! A member returning an const rvalue reference to the column indices.
	inline thrust::device_vector<int> col_ind() const&
	{
		return m_csr_col_ind_a;
	}

  private:
	thrust::device_vector<T> m_csr_val_a;
	thrust::device_vector<int> m_csr_row_ptr_a;
	thrust::device_vector<int> m_csr_col_ind_a;

	std::size_t m_rows{};
	std::size_t m_cols{};
};

//! A template class for matrices stored in gpu memory and column major layout.
template<typename T>
class device_matrix final
{
  public:
	using pointer = typename thrust::device_vector<T>::pointer;
	using const_pointer = typename thrust::device_vector<T>::const_pointer;
	using iterator = typename thrust::device_vector<T>::iterator;
	using const_iterator = typename thrust::device_vector<T>::const_iterator;
	using difference_type = typename thrust::iterator_difference<iterator>::type;
	using reference = typename thrust::device_vector<T>::reference;
	using const_reference = typename thrust::device_vector<T>::const_reference;
	using range = strided_range<iterator>;
	using const_range = strided_range<const_iterator>;

	//! Constructor.
	/*!
	 * \param i_rows The number of rows.
	 * \param i_cols The number of columns.
	 */
	constexpr explicit device_matrix(Rows i_rows = Rows{}, Cols i_cols = Cols{})
		: m_data(i_rows.value() * i_cols.value()), m_rows{i_rows.value()}, m_cols{i_cols.value()},
		  m_ld{i_rows.value()}
	{
	}

	//! Constructor.
	/*!
	 * \param i_size The matrix size.
	 * \param i_init Initial value.
	 */
	constexpr explicit device_matrix(Size i_size)
		: m_data(i_size.rows() * i_size.cols()), m_rows{i_size.rows()}, m_cols{i_size.cols()},
		  m_ld{i_size.rows()}
	{
	}

	//! Constructor.
	/*!
	 * \param i_rows The number of rows.
	 * \param i_cols The number of columns.
	 * \param i_init Initial value.
	 */
	constexpr explicit device_matrix(Rows i_rows, Cols i_cols, T i_init)
		: m_data(i_rows.value() * i_cols.value(), i_init), m_rows{i_rows.value()},
		  m_cols{i_cols.value()}, m_ld{i_rows.value()}
	{
	}

	//! Constructor.
	/*!
	 * \param i_size The matrix size.
	 * \param i_init Initial value.
	 */
	constexpr explicit device_matrix(Size i_size, T i_init)
		: m_data(i_size.rows() * i_size.cols(), i_init), m_rows{i_size.rows()},
		  m_cols{i_size.cols()}, m_ld{i_size.rows()}
	{
	}

	//! Constructor.
	/*!
	 * \param i_data The matrix data of size i_rows * i_cols.
	 * \param i_rows The number of rows.
	 * \param i_cols The number of columns.
	 * \param i_col_major If true the matrix is stored in column major format else in row major
	 * format.
	 */
	explicit device_matrix(thrust::device_vector<T> i_data, Rows i_rows, Cols i_cols)
		: m_data{std::move(i_data)}, m_rows{i_rows.value()}, m_cols{i_cols.value()},
		  m_ld{i_rows.value()}

	{
		Expects(m_data.size() == m_rows * m_cols);
	}

	//! Move constructor.
	constexpr device_matrix(device_matrix&&) = default;

	//! Move operator.
	device_matrix& operator=(device_matrix&&) = default;

	//! Copy constructor.
	device_matrix(const device_matrix&) = default;

	//! Copy operator.
	device_matrix& operator=(const device_matrix&) = default;

	//! A member returning the number of rows.
	inline constexpr std::size_t rows() const noexcept
	{
		return m_rows;
	}

	//! A member returning the number of columns.
	inline constexpr std::size_t cols() const noexcept
	{
		return m_cols;
	}

	//! A member returning the leading dimension.
	inline constexpr std::size_t ld() const noexcept
	{
		return m_ld;
	}

	//! A member returning the size of the matrix.
	inline constexpr Size size() const noexcept
	{
		return Size{Rows{m_rows}, Cols{m_cols}};
	}

	//! A member returning the total number of elements.
	inline constexpr std::size_t total() const noexcept
	{
		return m_rows * m_cols;
	}

	//! A member returning a device_ptr to the first element.
	inline pointer data() noexcept
	{
		return m_data.data();
	}

	//! A member returning a const device_ptr to the first element.
	inline const_pointer data() const noexcept
	{
		return m_data.data();
	}

	//! A member resizing the matrix. If new columns or rows are added they will be filled with 0.
	inline void resize(std::size_t i_rows, std::size_t i_cols) noexcept
	{
		// TODO
		throw std::logic_error{
			"Device_matrix::resize(std::size_t, std::size_t) is not implemented."};
	}

	//! A member returning a range for iterating over one row.
	inline range getRowRange(std::size_t i_row) noexcept
	{
		Expects(i_row < m_rows);

		const auto begin = m_data.begin() + gsl::narrow<difference_type>(i_row);
		const auto end = m_data.end();
		const auto stride = gsl::narrow<difference_type>(m_rows);

		return range{begin, end, stride};
	}

	//! A member returning a const range for iterating over one row.
	inline const_range getRowRange(std::size_t i_row) const noexcept
	{
		Expects(i_row < m_rows);

		const auto begin = m_data.cbegin() + gsl::narrow<difference_type>(i_row);
		const auto end = m_data.cend();
		const auto stride = gsl::narrow<difference_type>(m_rows);

		return const_range{begin, end, stride};
	}

	//! A member returning a range for iterating over one column.
	inline range getColRange(std::size_t i_col) noexcept
	{
		Expects(i_col < m_cols);

		const auto begin = m_data.begin() + gsl::narrow<difference_type>(i_col * m_rows);
		const auto end = begin + gsl::narrow<difference_type>(m_rows);
		const auto stride = gsl::narrow<difference_type>(1);

		return range{begin, end, 1};
	}

	//! A member returning a const range for iterating over one column.
	inline const_range getColRange(std::size_t i_col) const noexcept
	{
		Expects(i_col < m_cols);

		const auto begin = m_data.cbegin() + gsl::narrow<difference_type>(i_col * m_rows);
		const auto end = begin + gsl::narrow<difference_type>(m_rows);
		const auto stride = gsl::narrow<difference_type>(1);

		return const_range{begin, end, 1};
	}

	//! A member returning an iterator over all elements.
	inline iterator begin() noexcept
	{
		return m_data.begin();
	}

	//! A member returning an iterator over all elements.
	inline iterator begin() const noexcept
	{
		return m_data.begin();
	}

	//! A member returning a const iterator over all elements.
	inline const_iterator cbegin() const noexcept
	{
		return m_data.cbegin();
	}

	//! A member returning an iterator to element following the last element in the matrix.
	inline iterator end() noexcept
	{
		return m_data.end();
	}

	//! A member returning an iterator to element following the last element in the matrix.
	inline iterator end() const noexcept
	{
		return m_data.end();
	}

	//! A member returning a const iterator to element following the last element in the matrix.
	inline const_iterator cend() const noexcept
	{
		return m_data.cend();
	}

	//! Subscript operator. Out of bound accesses are undefined.
	inline reference operator[](std::size_t i_pos) & noexcept
	{
		return m_data[i_pos];
	}

	//! Subscript operator. Out of bound accesses are undefined.
	inline const_reference operator[](std::size_t i_pos) const& noexcept
	{
		return m_data[i_pos];
	}

	//! Bound checked element access.
	inline reference at(Rows i_row, Cols i_col) & noexcept
	{
		Expects(i_row.value() < m_rows);
		Expects(i_col.value() < m_cols);

		return m_data[i_col.value() * m_rows + i_row.value()];
	}

	//! Bound checked element access.
	inline const_reference at(Rows i_row, Cols i_col) const& noexcept
	{
		Expects(i_row.value() < m_rows);
		Expects(i_col.value() < m_cols);

		return m_data[i_col.value() * m_rows + i_row.value()];
	}

	//! A member returning the underlying container.
	inline thrust::device_vector<T> container() const
	{
		return m_data;
	}

  private:
	thrust::device_vector<T> m_data;

	std::size_t m_rows{};
	std::size_t m_cols{};
	std::size_t m_ld{};
};

//! A template function that copies a region of interest of a matrix to a new one.
template<typename T>
device_matrix<T> copy(const device_matrix<T>& i_matrix, const ROI& i_roi)
{
	auto o_matrix = device_matrix<T>{Rows{i_roi.rows()}, Cols{i_roi.cols()}};
	auto o_matrix_ptr = o_matrix.data().get();
	const auto i_matrix_ptr = i_matrix.data().get();

	const auto row_offset = i_roi.row_offset();
	const auto col_offset = i_roi.col_offset();
	const auto i_matrix_rows = i_matrix.rows();
	const auto o_matrix_rows = o_matrix.rows();
	const auto first = thrust::make_counting_iterator(std::size_t{0});
	const auto last = first + i_roi.cols();

	thrust::for_each(first, last, [=] __device__(std::size_t o_col) {
		const auto from = i_matrix_ptr + row_offset + (o_col + col_offset) * i_matrix_rows;
		const auto to = o_matrix_ptr + o_col * o_matrix_rows;
		thrust::copy_n(thrust::device, from, o_matrix_rows, to);
	});

	return o_matrix;
}

//! A struct that defines termination criteria for an algorithm.
struct term_criteria final
{
	explicit term_criteria(std::size_t i_max_iter = 1000, float i_eps = 10e-10f,
						   float i_entropy = 10e-3f)
		: max_iter{i_max_iter}, eps{i_eps}, entropy{i_entropy}
	{
		Expects(eps > 0.0f);
		Expects(entropy > 0.0f);
	}

	//! The maximum number of iterations.
	std::size_t max_iter{};
	//! The minimum value for the probabilities.
	float eps{};
	//! The threshold for the average entropy.
	float entropy{};
};

//! A struct that defines the parameters for the assignment filter.
struct assignment_param final
{
	explicit assignment_param(const Size& i_size_neigh = Size{Rows{3}, Cols{3}}, float i_rho = 0.3f,
							  bool i_use_tangent = true, float i_gamma = 1.0f)
		: size_neigh{i_size_neigh}, rho{i_rho}, use_tangent{i_use_tangent}, gamma{i_gamma}
	{
		Expects(size_neigh.rows() % 2 == 1);
		Expects(size_neigh.cols() % 2 == 1);
		Expects(rho > 0.0f);
		Expects(gamma >= 0.0f);
	}

	//! The neighborhood size for averaging. The integers have to be odd.
	Size size_neigh{};
	//! The selectivity parameter.
	float rho{};
	//! If true the tangent space approach is used.
	bool use_tangent{};
	//! A weighting parameter for the assignment update.
	float gamma{};
};