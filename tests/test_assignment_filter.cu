#include "../include/assignment_filter.h"
#include "../include/device_matrix.h"
#include "../include/distance_matrix.h"
#include "../include/label_image.h"
#include "../include/metric.h"
#include "../tests/test_helper.h"

#include <algorithm>
#include <array>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

using namespace std::chrono;
using namespace std::chrono_literals;
using namespace std::string_literals;

constexpr auto RESULT_PATH = "tests/results/";
constexpr auto IMAGES_PATH = "tests/images/";
constexpr auto MANDRILL_PATH = "mandrill.png";
constexpr auto LABELS_NAMES =
	std::array<const char*, 4>{"labels5", "labels10", "labels15", "labels20"};

constexpr auto NEIGH_SIZES = std::array<Size, 4>{Size{Rows{std::size_t{3}}, Cols{std::size_t{3}}},
												 Size{Rows{std::size_t{5}}, Cols{std::size_t{5}}},
												 Size{Rows{std::size_t{7}}, Cols{std::size_t{7}}},
												 Size{Rows{std::size_t{9}}, Cols{std::size_t{9}}}};

//! Computes the distance between the image and the labels.
device_matrix<float> get_distance_matrix(const thrust::device_vector<vec3<float>>& i_pixels,
										 const thrust::device_vector<vec3<float>>& i_labels)
{
	const auto t0 = high_resolution_clock::now();
	const auto d_D = distance_matrix<euclidean_metric>(i_pixels, i_labels);
	const auto elapsed = high_resolution_clock::now() - t0;

	return d_D;
}

//! Computes the distance between the image and the labels.
device_matrix<double> get_distance_matrix_dbl(const thrust::device_vector<vec3<float>>& i_pixels,
											  const thrust::device_vector<vec3<float>>& i_labels)
{
	const auto t0 = high_resolution_clock::now();
	const auto d_D = distance_matrix_dbl<euclidean_metric>(i_pixels, i_labels);
	const auto elapsed = high_resolution_clock::now() - t0;

	return d_D;
}

//! Returns the assignment probabilities for the given distance matrix and parameters.
device_matrix<float> get_assignment_probabilities(assignment_filter* i_filter,
												  const device_matrix<float>& i_D,
												  const Size& i_size_image,
												  std::stringstream& o_stream)
{
	// warm up
	i_filter->compute_assignment_prob(i_D, i_size_image);

	const auto t0_adj = high_resolution_clock::now();
	const auto unused = i_filter->compute_adj_matrix(i_size_image);
	const auto elapsed_adj = high_resolution_clock::now() - t0_adj;

	const auto t0_compute = high_resolution_clock::now();
	const auto d_W = i_filter->compute_assignment_prob(i_D, i_size_image);
	const auto elapsed_compute = high_resolution_clock::now() - t0_compute;

	o_stream << duration_cast<milliseconds>(elapsed_adj).count() << ","
			 << duration_cast<milliseconds>(elapsed_compute).count();

	return d_W;
}

//! Returns the assignment probabilities for the given distance matrix and parameters.
device_matrix<double> get_assignment_probabilities_dbl(assignment_filter_dbl* i_filter,
													   const device_matrix<double>& i_D,
													   const Size& i_size_image,
													   std::stringstream& o_stream)
{
	// warm up
	i_filter->compute_assignment_prob(i_D, i_size_image);

	const auto t0_adj = high_resolution_clock::now();
	const auto unused = i_filter->compute_adj_matrix(i_size_image);
	const auto elapsed_adj = high_resolution_clock::now() - t0_adj;

	const auto t0_compute = high_resolution_clock::now();
	const auto d_W = i_filter->compute_assignment_prob(i_D, i_size_image);
	const auto elapsed_compute = high_resolution_clock::now() - t0_compute;

	o_stream << duration_cast<milliseconds>(elapsed_adj).count() << ","
			 << duration_cast<milliseconds>(elapsed_compute).count();

	return d_W;
}

//! Only tests the float version.
void test_assignment_filter() noexcept
{
	try
	{
		std::cout << "Assignment filter test" << std::endl;
		auto result = std::stringstream{};
		result << "Neighborhood,Labels,Rho,Gamma,Eps,Entropy,Adjacency_ms,Iterations_ms";

		// Loading of data and clustering.
		const auto image =
			load_image(std::string(IMAGES_PATH) + MANDRILL_PATH, vec3<float>{}); // TODO
		const auto image_size = image.size();

		auto filter = assignment_filter::create();
		const auto term = term_criteria{1000, 1e-10f, 1e-3f};
		filter->set_termination_criteria(term);

		for(const auto labels_name : LABELS_NAMES)
		{
			for(const auto neigh_size : NEIGH_SIZES)
			{
				const auto labels_path = std::string(IMAGES_PATH) + labels_name + ".png";
				const auto labels = load_image(labels_path, vec3<float>{}).container();
				const auto param = assignment_param{neigh_size, 0.1f, true, 1.0f};
				filter->set_assignment_parameter(param);

				// Collect result and parameters in csv format.
				const auto neigh_string =
					std::to_string(neigh_size.rows()) + "x"s + std::to_string(neigh_size.cols());

				result << "\n"
					   << neigh_string << "," << labels.size() << "," << param.rho << ","
					   << param.gamma << "," << term.eps << "," << term.entropy << ",";

				// Assignment filter.
				const auto D = get_distance_matrix(image.container(), labels);
				const auto W = get_assignment_probabilities(filter.get(), D, image_size, result);

				// Compute and store the labeled image.
				const auto image_labeled = label_image(image_size, W, labels);
				const auto file_path =
					RESULT_PATH + "mandrill_"s + labels_name + "_"s + neigh_string + ".png"s;
				store_image(file_path, image_labeled);

				std::cout << "labels: " << labels_name << ", neigh: " << neigh_string << "\n";
			}
		}

		std::cout << std::endl;

		auto out = std::ofstream(RESULT_PATH + "benchmark.csv"s, std::ios::out | std::ios::trunc);
		out << result.str();
	}
	catch(const thrust::system_error& e)
	{
		std::cerr << e.what() << std::endl << e.code() << std::flush;
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << std::flush;
	}
	catch(...)
	{
		std::cerr << "Encountered an unknown exception." << std::flush;
	}
}